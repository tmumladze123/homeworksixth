package com.example.davalebaeqvsi

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.davalebaeqvsi.databinding.ActivityMainBinding


@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    var name:String="Tamari"
    var surname:String="Mumladze"
    var email:String="userbdofcs@Gmail.com"
    var year:String="2000"
    var gender:String="Female"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    fun clickChange(view: View)
    {
            val intent=Intent(this, MainActivity2::class.java)
            startActivityForResult(intent, 1 )        //startActivity(intent)

    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode==1)
        { if (resultCode == -1)
        {

            if(!data?.getStringExtra("NAME").toString().isEmpty())
                name= data?.getStringExtra("NAME").toString()
            if(!data?.getStringExtra("lastName").toString().isEmpty())
                surname= data?.getStringExtra("lastName").toString()
            if(!data?.getStringExtra("email").toString().isEmpty())
                email= data?.getStringExtra("email").toString()
            if(!data?.getStringExtra("birthYear").toString().isEmpty())
                year= data?.getStringExtra("birthYear").toString()
            if(!data?.getStringExtra("gender").toString().isEmpty())
                gender= data?.getStringExtra("gender").toString()
            binding.textViewName.setText(name)
        binding.textViewLastName.setText(surname)
        binding.textViewEmail.setText(email)
        binding.textViewBirthYear.setText(year)
        binding.textViewGender.setText(gender)

        }
        }
    }

}