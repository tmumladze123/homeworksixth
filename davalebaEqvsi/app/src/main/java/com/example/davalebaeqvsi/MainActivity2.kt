package com.example.davalebaeqvsi

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.davalebaeqvsi.databinding.ActivityMain2Binding


class MainActivity2 : AppCompatActivity() {
    private lateinit var binding: ActivityMain2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityMain2Binding.inflate(layoutInflater)
        setContentView(binding.root)
    }
    fun clickSave(view: View)
    {
        val intent= intent
        intent.putExtra("NAME",binding.Name.text.toString())
        intent.putExtra("lastName",binding.LastName.text.toString())
        intent.putExtra("email",binding.Email.text.toString());
        intent.putExtra("birthYear",binding.BirthYear.text.toString());
        intent.putExtra("gender",binding.Gender.text.toString());
        setResult(RESULT_OK, intent);
        finish()
    }
}